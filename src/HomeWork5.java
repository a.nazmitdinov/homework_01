import java.util.Scanner;

public class HomeWork5 {
    public static void main(String[] args) {
        final double CENTIMETERS_TO_INCH = 2.54; // 1 дюйм = 2,54 сантиметров
        double centimeter, inch;

        Scanner input = new Scanner(System.in);
        System.out.print("Введите количество сантиметров: ");

        centimeter = input.nextDouble();

        // Вычисление кол-ва дюймов в сантиметрах
        inch = CENTIMETERS_TO_INCH * centimeter;

        System.out.println("Результат вычисления: \n" + (int)centimeter +
                           " см. = " + inch + " дюймов");
    }
}
