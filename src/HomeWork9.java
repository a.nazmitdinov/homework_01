import java.util.Scanner;

public class HomeWork9 {
    public static void main(String[] args) {
        double n; // Бюджет мероприятия (в тугриках)
        double k; // Бюджет на одного гостя (в тугриках)
        int numberOfGuests; // Кол-во гостей

        Scanner input = new Scanner(System.in);
        System.out.print("Введите бюджет на мероприятие: ");
        n = input.nextDouble();
        System.out.print("Введите бюджет на одного гостя: ");
        k = input.nextDouble();

        // Вычисление возможного количества гостей
        numberOfGuests = (int)( n / k);

        System.out.println("Вы можете пригласить на мероприятие " + numberOfGuests + " гостей");

    }
}
