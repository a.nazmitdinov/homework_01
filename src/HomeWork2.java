import java.util.Scanner;

public class HomeWork2 {
    public static void main(String[] args) {
        double a, b; // целые числа
        double averageQuadratic; // среднее квадратическое

        Scanner input = new Scanner(System.in);

        System.out.println("Введите через пробел значения a и b: ");
        a = input.nextInt();
        b = input.nextInt();

        averageQuadratic = Math.sqrt((a * a) / 2 + (b * b) / 2) * 100/100.0; // вычисление значения среднего квадратического

        System.out.println("Среднее квадратическое чисел " + (int)a + " и " + (int)b + " равно: " + averageQuadratic);
    }
}
