import java.util.Scanner;

public class HomeWork4 {
    public static void main(String[] args) {
        final int SECONDS_PER_MINUTE = 60, MINUTES_PER_HOUR = 60;
        int count; // Кол-во секунд, прошедших с начала дня
        int totalMinutes, currentHour, currentMinutes; // Общее кол-во минут, а также тукущие значения минут и часов

        Scanner input = new Scanner(System.in);

        System.out.print("Введите количество секунд, прошедших с начала текущего дня: ");
        count = input.nextInt();

        // Вычислить общее кол-во минут, прошедших с начала дня
        totalMinutes = count / SECONDS_PER_MINUTE;

        // Вычислить текущее кол-во минут
        currentMinutes = totalMinutes % MINUTES_PER_HOUR;

        // Вычислить текущее кол-во часов
        currentHour = totalMinutes / MINUTES_PER_HOUR;

        // Отобразить текущее время
        System.out.println("Текущее время: " + currentHour + ":" + currentMinutes);
    }
}
