import java.util.Scanner;

public class HomeWork7 {
    public static void main(String[] args) {
        int n; // Целое двухзначное число
        int first, second; // Первая и вторая цифры цисла n

        Scanner input = new Scanner(System.in);
        System.out.print("Введите двухзначное число: ");

        n = input.nextInt();

        // Определение чисел из которых состоит число n
        first = n / 10;
        second = n % 10;

        System.out.println("Вы ввели число " + n + "\n" +
                "Результат перестановки его чисел: " + second + first);

    }
}
