import java.util.Scanner;

public class HomeWork6 {
     public static void main(String[] args) {
         final double KILOMETERS_IN_MILE = 1.60934; // 1 миля = 1,60934 км.
         double count; // Количество километров
         double miles; // Количество миль

         Scanner input = new Scanner(System.in);
         System.out.print("Введите количество километров: ");

         count = input.nextDouble();

         // Вычисление кол-ва миль в километрах
         miles = (1 / KILOMETERS_IN_MILE) * count;

         System.out.println("Результат вычисления: \n" + (int)count +
                            " км. = " + miles + " миль");
     }
}

