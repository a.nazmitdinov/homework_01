import java.util.Scanner;

public class HomeWork3 {
    public static void main(String[] args) {
        String userName; // Имя рользователя

        Scanner input = new Scanner(System.in);

        System.out.print("Введите имя пользователя: ");
        userName = input.next();

        System.out.println("Привет, " + userName + "!");
    }
}
