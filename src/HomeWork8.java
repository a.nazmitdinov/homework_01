import java.util.Scanner;

public class HomeWork8 {
    public static void main(String[] args) {
        final int DAYS_IN_A_MONTH = 30; // Количество дней
        int n; // Баланс счета в банке
        double budgetPerDay; // Дневной бюджет

        Scanner input = new Scanner(System.in);
        System.out.print("Введите баланс счета в банке: ");
        n = input.nextInt();

        budgetPerDay = (double)n / DAYS_IN_A_MONTH;

        System.out.println("Дневной бюджет на 30 дней равен: " + budgetPerDay);
    }
}

