import java.util.Scanner;

public class HomeWork1 {
    public static void main(String[] args) {
        double radius; // радиус щара
        double volume; // объем шара

        Scanner input = new Scanner(System.in);

        radius = input.nextDouble();

        volume = 4/3.0 * Math.PI * Math.pow(radius,3);

        System.out.println(volume);
    }
}
